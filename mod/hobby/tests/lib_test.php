<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Unit tests for (some of) mod/feedback/lib.php.
 *
 * @package    mod_hobby
 * @copyright  Lam Tran
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->dirroot . '/mod/hobby/lib.php');

/**
 * Unit tests for (some of) mod/hobby/lib.php.
 *
 * @copyright  Lam Tran
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_hobby_lib_testcase extends advanced_testcase {
    /**
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function test_hobby_initialise() {
        $this->resetAfterTest();
        $this->setAdminUser();

        $course = $this->getDataGenerator()->create_course();
        $params['course'] = $course->id;
        $params['name'] = 'Hobby';
        $params['intro'] = 'Hobby test';
        $hobby = $this->getDataGenerator()->create_module('hobby', $params);

        // Test different ways to construct the structure object.
        $pseudocm = get_coursemodule_from_instance('hobby', $hobby->id); // Object similar to cm_info.
        $cm = get_fast_modinfo($course)->instances['hobby'][$hobby->id]; // Instance of cm_info.

        $constructorparams = [
            [$hobby, null],
            [null, $pseudocm],
            [null, $cm],
            [$hobby, $pseudocm],
            [$hobby, $cm],
        ];

        foreach ($constructorparams as $params) {
            $structure = new mod_hobby_completion($params[0], $params[1], $course->id);
            $this->assertTrue($structure->can_complete());
            $this->assertTrue($structure->get_cm() instanceof cm_info);
            $this->assertEquals($hobby->course, $structure->get_courseid());
            $structureintro =  $structure->get_hobby() ?  $structure->get_hobby()->intro : null;
            $this->assertEquals($hobby->intro, $structureintro);
        }
    }

    /**
     * @throws coding_exception
     * @throws moodle_exception
     */
    public function test_hobby_item_create() {
        global $DB;
        $this->resetAfterTest();
        $this->setAdminUser();

        $course = $this->getDataGenerator()->create_course();
        $params['course'] = $course->id;
        $params['name'] = 'Hobby';
        $params['intro'] = 'Hobby test';
        $hobby = $this->getDataGenerator()->create_module('hobby', $params);

        $params = [
          'name' => 'Lam',
          'email' => 'tranbaolam6693@gmail.com',
          'phone' => '0123456789',
          'intro' => 'test',
          'hobby' => $hobby->id
        ];

        $DB->insert_record('hobby_item', (object) $params);
        $item = $DB->get_record('hobby_item', ['hobby' => $hobby->id]);
        $exist = $item ? true : false;

        $this->assertTrue($exist);

    }

    /**
     * @throws dml_exception
     */
    public function test_hobby_item_update() {
        global $DB;
        $this->resetAfterTest();
        $this->setAdminUser();

        $course = $this->getDataGenerator()->create_course();
        $params['course'] = $course->id;
        $params['name'] = 'Hobby';
        $params['intro'] = 'Hobby test';
        $hobby = $this->getDataGenerator()->create_module('hobby', $params);

        $params = [
            'name' => 'Lam',
            'email' => 'tranbaolam6693@gmail.com',
            'phone' => '0123456789',
            'intro' => 'test',
            'hobby' => $hobby->id
        ];

        $id = $DB->insert_record('hobby_item', (object) $params);

        $updateparams = [
            'name' => 'Lam update',
            'email' => 'tranbaolam6693update@gmail.com',
            'phone' => '01234567891',
            'intro' => 'test update',
            'hobby' => $hobby->id,
            'id' => $id
        ];

        $DB->update_record('hobby_item', (object) $updateparams);

        $item = $DB->get_record('hobby_item', ['hobby' => $hobby->id]);

        $this->assertEquals($updateparams['name'], $item->name);
        $this->assertEquals($updateparams['email'], $item->email);
        $this->assertEquals($updateparams['phone'], $item->phone);
        $this->assertEquals($updateparams['intro'], $item->intro);
        $this->assertEquals($updateparams['hobby'], $item->hobby);

    }

}
