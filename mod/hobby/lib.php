<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_hobby
 * @copyright   2019 Lam Tran
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Return if the plugin supports $feature.
 *
 * @param string $feature Constant representing the feature.
 * @return true | null True if the feature is supported, null otherwise.
 */
function hobby_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        default:
            return null;
    }
}

/**
 * @param $data
 * @param null $mform
 * @return bool|int
 * @throws dml_exception
 */
function hobby_add_instance($data, $mform = null) {
    global $DB;

    $data->timecreated = $data->timemodified = time();

    $id = $DB->insert_record('hobby', $data);

    return $id;
}

/**
 * @param $moduleinstance
 * @param null $mform
 * @return bool
 * @throws dml_exception
 */
function hobby_update_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timemodified = time();
    $moduleinstance->id = $moduleinstance->instance;

    return $DB->update_record('hobby', $moduleinstance);
}

/**
 * @param $id
 * @return bool
 * @throws dml_exception
 */
function hobby_delete_instance($id) {
    global $DB;

    $exists = $DB->get_record('hobby', array('id' => $id));

    if (!$exists) {
        return false;
    }

    try {
        $transaction = $DB->start_delegated_transaction();
        $DB->delete_records('hobby_item', array('hobby' => $id));
        $DB->delete_records('hobby', array('id' => $id));
        $transaction->allow_commit();
    }
    catch(\Exception $e) {
        if (!empty($transaction) && !$transaction->is_disposed()) {
            $transaction->rollback($e);
        }
    }

    return true;
}

/**
 * @param $page
 * @param $course
 * @param $cm
 * @param $context
 */
function hobby_view($page, $course, $cm, $context) {
    // Completion.
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
}
