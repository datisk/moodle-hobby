<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class mod_feedback_complete_form
 *
 * @package   mod_hobby
 * @copyright Lam Tran
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Class mod_hobby_complete_form
 *
 * @package   mod_hobby
 * @copyright Lam Tran
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_hobby_complete_form extends moodleform {

    /** @var mod_hobby_structure|mod_hobby_completion */
    protected $structure;

    /**
     * mod_hobby_complete_form constructor.
     *
     * @param mod_hobby_structure $structure
     * @param $formid
     * @param null $customdata
     */
    public function __construct(mod_hobby_structure $structure, $formid, $customdata = null) {
        $this->structure = $structure;
        parent::__construct(null, $customdata, 'POST', '',
                array('id' => $formid, 'class' => 'feedback_form'), true);
        $this->set_display_vertical();
    }

    /**
     * Can be used by the items to get the course id for which feedback is taken
     *
     * This function returns 0 for feedbacks that are located inside the courses.
     * $this->get_feedback()->course will return the course where feedback is located.
     * $this->get_current_course_id() will return the course where user was before taking the feedback
     *
     * @return int
     */
    public function get_course_id() {
        return $this->structure->get_courseid();
    }

    /**
     * @return mixed
     * @throws dml_exception
     */
    public function get_hobby() {
        return $this->structure->get_hobby();
    }

    /**
     * @return stdClass
     * @throws dml_exception
     */
    public function get_hobby_item() {
        return $this->structure->get_hobby_item();
    }

    /**
     * @return stdClass
     */
    public function get_cm() {
        return $this->structure->get_cm();
    }

    /**
     * @return mixed
     * @throws dml_exception
     */
    public function get_current_course_id() {
        return $this->structure->get_courseid() ?: $this->get_hobby()->course;
    }

    /**
     * @throws coding_exception
     * @throws dml_exception
     */
    protected function definition() {
        $mform = $this->_form;

        // Adding the "general" fieldset, where all the common settings are showed.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('hidden', 'id', $this->get_cm()->id);
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'courseid', $this->get_current_course_id());
        $mform->setType('courseid', PARAM_INT);

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('hobbyname', 'mod_hobby'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('text', 'email', get_string('hobbyemail', 'mod_hobby'), array('size' => '64'));
        $mform->setType('email', PARAM_TEXT);
        $mform->addRule('email', null, 'required', null, 'client');
        $mform->addRule('email', get_string('hobbyemailformat', 'mod_hobby'), 'email', null, 'client');
        $mform->addRule('email', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('text', 'phone', get_string('hobbyphone', 'mod_hobby'), array('size' => '64'));
        $mform->setType('phone', PARAM_TEXT);
        $mform->addRule('phone', null, 'required', null, 'client');
        $mform->addRule('phone', get_string('hobbyphoneformat', 'mod_hobby'), 'numeric', null, 'client');
        $mform->addRule('phone', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('editor', 'intro', get_string('hobbyintro', 'mod_hobby'), array('rows' => 10));
        $mform->setType('intro', PARAM_RAW); // no XSS prevention here, users must be trusted
        $mform->addRule('intro', get_string('required'), 'required', null, 'client');

        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'savevalues', get_string('hobby:save', 'hobby'),
                array('class' => 'form-submit'));
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');

    }


}
